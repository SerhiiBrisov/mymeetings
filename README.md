# Run API

To run server open `.sln` file with Microsoft Visual Studio and `Start without debugging` to start IIS server'

## Install npm packages

Open CLI in Angular root folder and execute command: `npm install` to generate npm-modules

# Run Client app

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build Localizations

Run `npm run build-locale:en && npm run build-locale:ru && npm run build-locale:uk` to build localizations of the project. The build artifacts will be stored in the `dist/` directory.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the project exploration contact me on https://t.me/SergeiB3123109.

