import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(){
    return browser.get('/');
  }

  getTitleText() {
    return element(by.tagName('h1')).getText();
  }


  getSubmitButton() {
    return element(by.cssContainingText('button', 'Submit'));
  }

  getNameField() {
    return element(by.id('Name'));
  }

  getDateField() {
    return element(by.id('Date'));  }

  getDetailsField() {
    return element(by.id('Details'));  }

  getToastr() {
    return element(by.className('ngx-toastr')).getText();  }
  
}
