import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Title Notes Managment', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Notes Managment');
  });

  it('should be message successful', () => {
    page.navigateTo();

    page.getNameField().sendKeys("test");
    page.getDateField().sendKeys("20/12/12");
    page.getDetailsField().sendKeys("test details");

    page.getSubmitButton().click();
    browser.sleep(1000);


    expect(page.getToastr()).toEqual('Submitted successfully');

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
