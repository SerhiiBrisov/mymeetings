import{
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
} from '@ngrx/store';
import { noteReducer } from './note-reducer'
import { NoteState  } from './note-reducer'

import {environment} from '../../../environments/environment';

export interface State{
    noteState:NoteState
}

export const reducers: ActionReducerMap<State> ={
    noteState : noteReducer
}

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
    return function (state: State, action: any): State {
        console.log('state', state);
        console.log('action', action);
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<State>[] = [logger];
