import { NoteDetail } from '../../shared/models/note-detail.model'
import { Action } from '@ngrx/store';
import * as noteAction from '../actions/note.action';


export const idNode = 'id';
export const nameNode = 'name';
export const dateNode = 'date';
export const detailsNode = 'details';
export const noteNode = 'note';

export interface NoteState {
    note: {
        id : number,
        name: string,
        date: string,
        details: string
    }
}

const initialState: NoteState = {
    note: {
        id : 0,
        name: '',
        date: '',
        details: ''
    }
}


export function noteReducer(state = initialState, action: noteAction.NoteActions) {
    switch (action.type) {
        case noteAction.ID:
            return {
                ...state,
                id: action.payload
            };
            break;

        case noteAction.NAME:
            return {
                ...state,
                name: action.payload
            };
            break;
        case noteAction.DATE:
            return {
                ...state,
                date: action.payload
            };
            break;
        case noteAction.DETAILS:
            return {
                ...state,
                details: action.payload
            };
            break;
        default:
            return state
            break;
    }

}