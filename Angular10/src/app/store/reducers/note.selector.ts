import {createFeatureSelector , createSelector} from '@ngrx/store'
import { NoteState, nameNode, dateNode, detailsNode, noteNode, idNode} from './note-reducer'
import { NoteDetail } from 'src/app/shared/models/note-detail.model';

export const selectIdFeature = createFeatureSelector<NoteState>(idNode);
export const selectNameFeature = createFeatureSelector<NoteState>(nameNode);
export const selectDateFeature = createFeatureSelector<NoteState>(dateNode);
export const selectDetailsFeature = createFeatureSelector<NoteState>(detailsNode);
export const selectNoteFeature = createFeatureSelector<NoteState>(noteNode);

export const selectNote = createSelector(
    selectNoteFeature,
    (state:NoteState):  object => state.note
);

export const selectId = createSelector (
    selectIdFeature,
    (state:NoteState): number =>state.note.id
);

export const selectName = createSelector (
    selectNameFeature,
    (state:NoteState): string =>state.note.name
);

export const selectDate = createSelector (
    selectDateFeature,
    (state:NoteState): string =>state.note.date
);

export const selectDetails = createSelector (
    selectDetailsFeature,
    (state:NoteState): string =>state.note.details
);

