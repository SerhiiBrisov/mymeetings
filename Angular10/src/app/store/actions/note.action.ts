import { Action  } from '@ngrx/store';
import { NoteDetail } from '../../shared/models/note-detail.model';


export const ID = '[ID] change';
export const NAME = '[NAME] change';
export const DATE = '[DATE] change';
export const DETAILS = '[DETAILS] change';

export class IdChange implements Action {
    readonly type = ID;

    constructor(public payload: number) { }
}

export class NameChange implements Action {
    readonly type = NAME;

    constructor(public payload: string) { }
}

export class DateChange implements Action {
    readonly type = DATE;

    constructor(public payload: string) { }
}

export class DetailsChange implements Action {
    readonly type = DETAILS;

    constructor(public payload: string) { }
}

export type NoteActions = IdChange | NameChange | DateChange | DetailsChange;