export class NoteDetail {
    IdNote: number;
    Name: string;
    Details: string;
    Date: string;
    constructor(
        IdNote: number,
        Name: string,
        Details: string,
        Date: string) { }
}
