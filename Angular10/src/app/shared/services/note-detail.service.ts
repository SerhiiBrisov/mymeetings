import { Injectable } from '@angular/core';
import { NoteDetail } from '../models/note-detail.model';
import { HttpClient } from "@angular/common/http";
import { Store, select } from '@ngrx/store';

import { NoteState } from '../../store/reducers/note-reducer';

@Injectable({
  providedIn: 'root'
})
export class NoteDetailService {

  formData:NoteDetail
    readonly rootURL = 'http://localhost:50912/api';
    list : NoteDetail[];
  
    constructor(private http: HttpClient,
      private store$: Store<NoteState>
      
      ) { }
  
    postNoteDetail() {
      return this.http.post(this.rootURL + '/NoteDetail', this.formData);
    }
    putNoteDetail() {
      return this.http.put(this.rootURL + '/NoteDetail/'+ this.formData.IdNote, this.formData);
    }
    deleteNoteDetail(id) {
      return this.http.delete(this.rootURL + '/NoteDetail/'+ id);
    }
  
    refreshList(){
      this.http.get(this.rootURL + '/NoteDetail')
      .toPromise()
      .then(res => this.list = res as NoteDetail[]);
    }
  }