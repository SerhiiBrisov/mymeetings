import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';


import { AppComponent } from './app.component';
import { NoteDetailsComponent } from './note-details/note-details.component';
import { NoteDetailComponent } from     './note-details/note-detail/note-detail.component';
import { NoteDetailListComponent } from './note-details/note-detail-list/note-detail-list.component';
import { NoteDetailService} from './shared/services/note-detail.service';
import { StoreModule } from '@ngrx/store';
import { reducers , metaReducers} from './store/reducers/index';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    NoteDetailsComponent,
    NoteDetailComponent,
    NoteDetailListComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),

  ],
  providers: [NoteDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
