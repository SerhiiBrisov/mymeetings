import { NoteDetail } from './../../shared/models/note-detail.model';
import { NoteDetailService } from './../../shared/services/note-detail.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { NoteState } from 'src/app/store/reducers/note-reducer';
import { IdChange } from 'src/app/store/actions/note.action';

@Component({
  selector: 'app-note-detail-list',
  templateUrl: './note-detail-list.component.html',
  styles: []
})
export class NoteDetailListComponent implements OnInit {

  constructor(public service: NoteDetailService,
    private toastr: ToastrService,
    public store$: Store<NoteState>) {

     }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(nd: NoteDetail) {
    this.service.formData = Object.assign({}, nd);
  }

  onDelete(IdNote) {
    if (confirm('Are you sure to delete this note ?')) {   
      this.store$.dispatch(new IdChange(IdNote));
      this.service.deleteNoteDetail(IdNote)
        .subscribe(res => {
          debugger;
          this.service.refreshList();
          this.toastr.warning('Deleted successfully', 'Note Register');
        },
          err => {
            debugger;
            console.log(err);
          })
    }
  }

}
