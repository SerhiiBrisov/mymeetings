import { Component, OnInit } from '@angular/core';
import { NoteDetailService } from 'src/app/shared/services/note-detail.service';
import { NgForm } from '@angular/forms';
import { NoteState } from '../../store/reducers/note-reducer';
import { ToastrService } from 'ngx-toastr';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectName, selectId } from '../../store/reducers/note.selector';
import { selectDate } from '../../store/reducers/note.selector';
import { selectDetails } from '../../store/reducers/note.selector';
import { IdChange, DateChange, NameChange, DetailsChange } from 'src/app/store/actions/note.action';


@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styles: []
})

export class NoteDetailComponent implements OnInit {

  
  public id$: Observable<number> = this.store$.pipe(select(selectId));
  public name$: Observable<string> = this.store$.pipe(select(selectName));
  public date$: Observable<string> = this.store$.pipe(select(selectDate));
  public details$: Observable<string> = this.store$.pipe(select(selectDetails));


  constructor(public service:NoteDetailService,
    private toastr: ToastrService,
    public store$: Store<NoteState>) { }

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      IdNote: 0,
      Name: '',
      Date: '',
      Details: ''
    }
  }

  onSubmit(form: NgForm) {
    
    this.store$.dispatch(new IdChange(this.service.formData.IdNote));
    this.store$.dispatch(new NameChange(this.service.formData.Name));
    this.store$.dispatch(new DateChange(this.service.formData.Date));
    this.store$.dispatch(new DetailsChange(this.service.formData.Details));

    if (this.service.formData.IdNote == 0)
      this.insertRecord(form),
      res =>{
        debugger;
      }
    else
      this.updateRecord(form);
  }

  insertRecord(form: NgForm) {
    this.service.postNoteDetail().subscribe(
      res => {        
        debugger;
        this.resetForm(form);
        this.toastr.success('Submitted successfully');   
        this.service.refreshList();
      },
      err => {
        debugger;
        console.log(err);
      }
    )
  }
  updateRecord(form: NgForm) {
    this.service.putNoteDetail().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.info('Submitted successfully', 'Note Updated');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }
    )
  }
}
