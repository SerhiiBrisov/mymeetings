﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YNotes.Context;
using YNotes.Models;
using YNotes.Services;

namespace YNotes.Controllers
{
    /// <summary>
    /// Class controller receives and process the request, passes the request data to it. 
    /// The controller processes this data and sends back the processing result.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    [Route("api/[controller]")]
    [ApiController]
    public class NoteDetailController : ControllerBase
    {
        /// <summary>
        /// Holds the value of the database context
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly NoteDetailContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoteDetailController"/> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <owner>Serhii Brysov</owner>
        public NoteDetailController(NoteDetailContext context) => this.dbContext = context;        

        /// <summary>
        /// Processes request of type GET without parameter, gets list of notes
        /// </summary>
        /// /// <owner>Serhii Brysov</owner>
        //
        // GET: api/NoteDetail
        //
        [HttpGet]
        public IEnumerable<NoteDetail> GetNotesDetails()
        {
            this.dbContext.NoteDetails.ToList().Sort(new NoteComparer());
            return this.dbContext.NoteDetails;
        }

        /// <summary>
        /// Processes request of type GET with parameter, gets detail of one note
        /// </summary>
        /// <param name="id">The database context.</param>
        /// <owner>Serhii Brysov</owner>
        //
        // GET: api/NoteDetail
        //
        [HttpGet("{id}")]
        public ActionResult<NoteDetail> GetNoteDetail(int id)
        {
            var noteDetail = this.dbContext.NoteDetails.ToList().Find(note => note.IdNote == id);
            if (noteDetail == null)
            {
                return NotFound();
            }
            return noteDetail;
        }

        /// <summary>
        /// Processes request of type POST, adds one new note
        /// </summary>
        /// <param name="noteDetail">The new note.</param>
        /// <owner>Serhii Brysov</owner>
        //
        // POST: api/NoteDetail
        //
        [HttpPost]
        public ActionResult<NoteDetail> PostNoteDetail(NoteDetail noteDetail)
        {
            this.dbContext.NoteDetails.Add(noteDetail);
            this.dbContext.SaveChanges();
            return CreatedAtAction("GetNoteDetail", new { id = noteDetail.IdNote }, noteDetail);

        }

        /// <summary>
        /// Processes request of type PUT, modifies note
        /// </summary>
        /// <param name="noteDetail">The modified note.</param>
        /// <param name="id">The id of note to modify</param>
        /// <owner>Serhii Brysov</owner>
        //
        // PUT: api/NoteDetail/5
        //
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNoteDetail(int id, NoteDetail noteDetail)
        {
            if (id != noteDetail.IdNote)
            {
                return BadRequest();
            }
            this.dbContext.Entry(noteDetail).State = EntityState.Modified;
            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!this.dbContext.NoteDetails.Any(e => e.IdNote == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Processes request of type DELETE, deletes one note
        /// </summary>
        /// <param name="id">The id of note to delete.</param>
        /// <owner>Serhii Brysov</owner>
        //
        // DELETE: api/NoteDetail/5
        //
        [HttpDelete("{id}")]
        public ActionResult<NoteDetail> DeleteNoteDetail(int id)
        {
            var noteDetail = this.dbContext.NoteDetails.ToList().Find(note => note.IdNote == id);
            if (noteDetail == null)
            {
                return NotFound();
            }
            this.dbContext.NoteDetails.Remove(noteDetail);
            this.dbContext.SaveChanges();
            return noteDetail;
        }
    }
}