using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using YNotes.Context;

namespace YNotes
{
    /// <summary>
    /// This class configures the application, configures the services that the application will use, installs components to process the request.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class Startup
    {
        /// <summary>
        /// Holds the connection string.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private const string connectionString = "DefaultConnection";

        /// <summary>
        /// Holds the client hosting server address.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private const string hostAddress = "http://localhost:4200";

        /// <summary>
        /// Constructor of this class, specifies the configuration
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public Startup(IConfiguration configuration) => Configuration = configuration;

        /// <summary>
        /// Gets <see cref="Configuration"/>.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The Configuration</value>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                    options.JsonSerializerOptions.PropertyNamingPolicy = null;
                });
            services.AddDbContext<NoteDetailContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString(connectionString)));
            services.AddCors();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(options =>
            options.WithOrigins(hostAddress)
            .AllowAnyMethod()
            .AllowAnyHeader()
            );
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}