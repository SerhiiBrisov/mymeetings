﻿using System;
using System.Collections.Generic;
using System.Globalization;
using YNotes.Models;

namespace YNotes.Services
{
    /// <summary>
    /// Custom comparer for <see cref="NoteDetail"/> 
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class NoteComparer : IComparer<NoteDetail>
    {
        /// <summary>
        /// Compares two notes
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="x">First note to compare</param>
        /// <param name="y">Second note to compare</param>
        /// <returns>
        /// 1  if first date earlier that second
        /// -1 if first date later that second
        /// 0  if dates are same and first letter of names equals
        /// </returns>
        public int Compare(NoteDetail x, NoteDetail y)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            if (DateTime.Parse(x.Date, culture) < DateTime.Parse(y.Date, culture))
            {
                return 1;
            }
            else if (DateTime.Parse(x.Date) > DateTime.Parse(y.Date))
            {
                return -1;
            }
            else
            {
                if (x.Name[0] > y.Name[0])
                    return 1;
                else if (x.Name[0] < y.Name[0])
                    return -1;
                else
                    return 0;
            }
        }
    }
}
