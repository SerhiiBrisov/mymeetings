﻿using Microsoft.EntityFrameworkCore;
using YNotes.Models;

namespace YNotes.Context
{
    /// <summary>
    /// Class implements database context 
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class NoteDetailContext : DbContext
    {
        /// <summary>
        /// Gets or sets <see cref="DbSet{NoteDetail}"/>
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The dbset for <see cref="NoteDetail"/></value>
        public DbSet<NoteDetail> NoteDetails { get; set; }

        /// <summary>
        /// Initializes new instance of database context with given options
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="options">The options of dbcontext</param>
        public NoteDetailContext(DbContextOptions<NoteDetailContext> options) : base(options)
        {

        }

    }
}
