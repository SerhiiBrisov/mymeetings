﻿using System.ComponentModel.DataAnnotations;

namespace YNotes.Models
{
    /// <summary>
    /// Entity class describing note.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class NoteDetail
    {
        /// <summary>
        /// Gets or sets the id primary key.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The id.</value>
        [Key]
        public int IdNote { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The name.</value>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The details.</value>
        public string Details { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <value>The date.</value>
        public string Date { get; set; }

    }
}
