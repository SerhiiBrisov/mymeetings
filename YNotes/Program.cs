using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace YNotes
{
    /// <summary>
    /// Main class in Web Api 
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public static class Program
    {
        /// <summary>
        /// Entry point of application
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Connecting configuration in a <see cref="Startup"/> class
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
